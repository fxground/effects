/*
	!Important!
	Don't change the code below!
 */
;(function(window, document) {
	var WP_location = '//fxground.com';
	var doc 		= document;
	var win 		= window;
    var html 		= doc.documentElement;
    var message     = {};
	var frame;
	var origin;

	win.addEventListener('message', function(event) {
		var iframeHeight = doc.getElementsByClassName('wrapper')[0].scrollHeight;

		if (event.origin.indexOf(WP_location) > -1) {
			frame = event.source;
			origin = event.origin;

			if (event.data == 'return height') {
				message.name = 'change height';
				message.height = iframeHeight;

				frame.postMessage(message, origin);
			} else if (event.data == 'hook click event') {
				frame.postMessage('click hooked', origin);
			}

		} else {
			return;
		}
	}, false);

	if (html.classList.contains('effect-preview')){
		doc.addEventListener('mouseup', function(event) {
			if ((event.ctrlKey || event.metaKey) && event.which == 1) {
				frame.postMessage('change location in new tab', origin);
			} else if (event.which == 2) {
				frame.postMessage('change location in new tab', origin);
			} else {
				frame.postMessage('change location', origin);
			};
		}, false);
	} else {
		doc.addEventListener('touchstart', function() {},false);
	}
})(window, document);

//Enable this code if you need to use jQuery
// ;(function(window, document, $) {
// 	var $win = $(window);
// 	var $doc = $(document);

// 	// Your code goes here...
// 	// jQuery.ready is no longer needed
// })(window, document, window.jQuery);
