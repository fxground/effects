/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "../src/assets/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../node_modules/fx-template-js/template.js":
/*!**************************************************!*\
  !*** ../node_modules/fx-template-js/template.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

;(function(window, document) {
	var WP_location = '//fxground.com';
	var doc 		= document;
	var win 		= window;
    var html 		= doc.documentElement;
    var message     = {};
	var frame;
	var origin;

	win.addEventListener('message', function(event) {
		var iframeHeight = doc.getElementsByClassName('wrapper')[0].scrollHeight;

		if (event.origin.indexOf(WP_location) > -1) {
			frame = event.source;
			origin = event.origin;

			if (event.data == 'return height') {
				message.name = 'change height';
				message.height = iframeHeight;

				frame.postMessage(message, origin);
			} else if (event.data == 'hook click event') {
				frame.postMessage('click hooked', origin);
			}

		} else {
			return;
		}
	}, false);

	if (html.classList.contains('effect-preview')){
		doc.addEventListener('mouseup', function(event) {
			if ((event.ctrlKey || event.metaKey) && event.which == 1) {
				frame.postMessage('change location in new tab', origin);
			} else if (event.which == 2) {
				frame.postMessage('change location in new tab', origin);
			} else if (event.which == 1) {
				frame.postMessage('change location', origin);
			} else {
				return;
			}
		}, false);
	} else {
		doc.addEventListener('touchstart', function() {},false);
	}
})(window, document);


/***/ }),

/***/ "../src/assets/js/main.js":
/*!********************************!*\
  !*** ../src/assets/js/main.js ***!
  \********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var fx_template_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fx-template-js */ "../node_modules/fx-template-js/template.js");
/* harmony import */ var fx_template_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fx_template_js__WEBPACK_IMPORTED_MODULE_0__);


(function (window, document, $) {
  var $win = $(window);
  var $doc = $(document);
  $('.loader--candys .loader__bar > span').each(function () {
    $(this).data('origWidth', $(this).width()).width(0).animate({
      width: $(this).data('origWidth')
    }, 1200);
  });
  var $loaderStepped = $('.loader--stepped');
  $loaderStepped.each(function () {
    var $step = $loaderStepped.find('.loader__bar-step');
    var steps = [];
    var $laoderPercent = $loaderStepped.find('.loader__bar-percent');
    $step.each(function () {
      var $this = $(this);
      steps.push($this);
      $this.on('click', function (x) {
        progress(x.target.id);
      });
    });

    function progress(stepNum) {
      var p = stepNum * 30;
      $laoderPercent[0].style.width = "".concat(p, "%");
      $.each(steps, function (index, value) {
        var $this = $(this);
        var $thisId = $this.attr('id');

        if ($thisId === stepNum) {
          $this.addClass('selected');
          $this.removeClass('completed').next().removeClass('selected, completed');
        }

        if ($thisId < stepNum) {
          $this.addClass('completed');
          $this.removeClass('selected', 'completed');
        }

        if ($thisId > stepNum) {
          $this.removeClass('selected', 'completed');
        }
      });
    }
  });
  var $progress = $('#progress');
  var $progressVal = $('#progress').val();
  var $loaderBar = $('.loader--gif');

  function increase() {
    var i = 1;

    if (i < 100) {
      i++;
      $progress.text(i + '%');
    }
  }

  function decrease() {
    var i = 100;

    if (i = 100) {
      console.log(i);
      i--;
      $progress.text(i + '%');
    }
  }

  decrease();
  $loaderBar.on('mouseenter', function () {
    setInterval(increase, 35);
  });
  $loaderBar.on('mouseleave', function () {
    setInterval(decrease, 35);
  });
})(window, document, window.jQuery);

/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map