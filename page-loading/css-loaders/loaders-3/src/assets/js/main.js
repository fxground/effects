/*
	!Important!
	Don't change the code below!
 */
import 'fx-template-js';

(function(window, document, $) {
	var $win = $(window);
	var $doc = $(document);

	$('.loader--candys .loader__bar > span').each(function() {
		$(this)
			.data('origWidth', $(this).width())
			.width(0)
			.animate(
				{
					width: $(this).data('origWidth')
				},
				1200
			);
	});

	//Stepped Loader

	const $loaderStepped = $('.loader--stepped');

	$loaderStepped.each(function() {
		let $step = $loaderStepped.find('.loader__bar-step');
		let steps = [];
		let $laoderPercent = $loaderStepped.find('.loader__bar-percent');

		$step.each(function() {
			var $this = $(this);

			steps.push($this);

			$this.on('click', function(x) {
				progress(x.target.id);
			});
		});

		function progress(stepNum) {
			let p = stepNum * 30;

			$laoderPercent[0].style.width = `${p}%`;

			$.each(steps, function(index, value) {
				var $this = $(this);
				var $thisId = $this.attr('id');

				if ($thisId === stepNum) {
					$this.addClass('selected');

					$this
						.removeClass('completed')
						.next()
						.removeClass('selected, completed');
				}

				if ($thisId < stepNum) {
					$this.addClass('completed');

					$this.removeClass('selected', 'completed');
				}

				if ($thisId > stepNum) {
					$this.removeClass('selected', 'completed');
				}
			});
		}
	});

	//Gif Loader

	const $progress = $('#progress');
	const $progressVal = $('#progress').val();
	const $loaderBar = $('.loader--gif');

	function increase() {
		let i = 1;

		if (i < 100) {
			i++;
			$progress.text(i + '%');
		}
	}

	function decrease() {
		let i = 100;

		if ((i = 100)) {
			console.log(i);
			i--;
			$progress.text(i + '%');
		}
	}

	decrease();

	$loaderBar.on('mouseenter', function() {
		setInterval(increase, 35);
	});

	$loaderBar.on('mouseleave', function() {
		setInterval(decrease, 35);
	});
})(window, document, window.jQuery);
