# FX ground effect template

Type: PostCSS

Files:

* `effect.html` -- list all effects here. Copy/paste the `.sampe` div. We have this in different sizes.
* `preview.html` -- this file will be used for the preview on the homepage. Copy/paste only one effect once you're done.
* `test-homepage.html` -- These files are for testing only. Once a new effect is completed, link it in the iframes and check if everything is Okay.
* `test-inner.html` -- These files are for testing only. Once a new effect is completed, link it in the iframes and check if everything is Okay.

The effects in this repo are only for demo. Please remove them once you start working.
.

---

# Sizes

Grid 3x3:
`effect-3x3.html`

Size: `width: 340px; height: 255px;`

Grid 2x2:
`effect-2x2.html`


Size: `width: 693px; height: 666px;`

Grid 1x1:
`effect-1x1.html`

Size: `width: 1047px; height: 1006px;`

---

# File structure:

```
- form elements
- parallax
- simple elements
    |
    ├── buttons
    |  ├── button 1
    |  └── button 2
    ├── links
    └── images

```

---

# Testing

Test your effect in:
`test-homepage.html`
`test-inner.html`

Note that these files are static downloads of the site. If they don't work you can easily download the pages your self.

You need to update the iframe sources..


---

If you encounter any bugs or difficulties, please contact npanajotov, zoran, mapostolov or niliev.
