/*
	!Important!
	Don't change the code below!
 */
import "fx-template-js";

//Enable this code if you need to use jQuery
;(function(window, document, $) {
	var $win = $(window);
	var $doc = $(document);

	/**
	 * Rating init
	 */
	var checkedRating = $('.rating input:checked');

	$('.rating input').not(':checked').on('mouseenter', function () {
		var $this = $(this);
		$this.prop('checked', true);
	}).on('mouseleave', function () {
		checkedRating.siblings('input').prop('checked', false);
		checkedRating.prop('checked', true);
	});

})(window, document, window.jQuery);
