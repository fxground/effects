/*
	!Important!
	Don't change the code below!
 */
import "fx-template-js";

//Enable this code if you need to use jQuery
;(function(window, document, $) {
	var $win = $(window);
	var $doc = $(document);

	/**
	 * Rating 1
	 */
	var checkedRating = $('.rating-1 input:checked');

	$('.rating-1 input').not(':checked').on('mouseenter', function () {
		var $this = $(this);
		$this.prop('checked', true);
	}).on('mouseleave', function () {
		checkedRating.siblings('input').prop('checked', false);
		checkedRating.prop('checked', true);
	});

	/**
	 * Rating 3
	 */
	$('.rating-3 .rate-half').hover(function() {
		var thisIndex = $(this).index(),
			parent = $(this).parent(),
			parentIndex = parent.index(),
			ranks = $('.rate');
		for (var i = 0; i < ranks.length; i++) {
			if(i < parentIndex) {
				$(ranks[i]).removeClass('half').addClass('full');
			} else {
				$(ranks[i]).removeClass('half').removeClass('full');
			}
		}
		if(thisIndex == 0) {
			parent.addClass('half');
		} else {
			parent.addClass('full');
		}
	});

	/**
	 * Rating 4
	 */
	var $ratingState = $('.rating-state');
	var $activeInput = $('.rating-4 input');

	var defaultActiveIndex = $activeInput.val();
	var defaultActiveElement = $('.rating-4 span')[defaultActiveIndex];

	$(defaultActiveElement).addClass('active');

	onHoverSpan(defaultActiveElement);

	function onHoverSpan(el) {
		var $this = $(el);
		var ratingState = $this.data('rating-value');

		$this.parent().addClass('hover');
		$this.siblings().removeClass('current').removeClass('hover');
		$this.addClass('hover').addClass('current')
			.prevAll().addClass('hover');

		$ratingState.text(ratingState);
	}

	$('.rating-4').on('mouseenter', 'span:not(.rating-state)', function () {
		onHoverSpan(this);
	}).on('mouseleave', function () {
		var $this = $(this);
		$this.removeClass('hover');

		var $active = $this.find('.active');
		var ratingState = $active.data('rating-value');

		$active.siblings().removeClass('current').removeClass('hover');
		$active.addClass('hover').removeClass('current')
			.prevAll().addClass('hover');

		$ratingState.text(ratingState);
	}).on('click', 'span', function (event) {
		var $this = $(this);
		$activeInput.val($this.index());
		$this.siblings().removeClass('active');
		$this.addClass('active');
   });
})(window, document, window.jQuery);

/**
 * Rating 2
 * @constructor
 */
function StarRating() {
	this.init();
};

/**
 * Initialize
 */
StarRating.prototype.init = function() {
	this.stars = document.querySelectorAll('.rating-2 span');
	for (var i = 0; i < this.stars.length; i++) {
		this.stars[i].setAttribute('data-count', i);
		this.stars[i].addEventListener('mouseenter', this.enterStarListener.bind(this));
	}

	document.querySelector('.rating-2').addEventListener('mouseleave', this.leaveStarListener.bind(this));
};

/**
 * This method is fired when a user hovers over a single star
 * @param e
 */
StarRating.prototype.enterStarListener = function(e) {
	this.fillStarsUpToElement(e.target);
};

/**
 * This method is fired when the user leaves the .rating-2 element, effectively removing all hover states.
 */
StarRating.prototype.leaveStarListener = function() {
	this.fillStarsUpToElement(null);
};

/**
 * Fill the star ratings up to a specific position.
 * @param el
 */
StarRating.prototype.fillStarsUpToElement = function(el) {
  // Remove all hover states:
	for (var i = 0; i < this.stars.length; i++) {
		if (el == null || this.stars[i].getAttribute('data-count') > el.getAttribute('data-count')) {
			this.stars[i].classList.remove('hover');
		} else {
			this.stars[i].classList.add('hover');
		}
	}
};

// Run:
new StarRating();
