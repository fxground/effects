/*
	!Important!
	Don't change the code below!
 */
import "fx-template-js";

// Enable this code if you need to use jQuery
;(function(window, document, $) {
	var $win = $(window);
	var $doc = $(document);

	$('.form-active input').blur(function(event) {
		if( $(this).val() ) {
			$(this).addClass('active');
    	} else {
			$(this).removeClass('active');
    	}
	});

})(window, document, window.jQuery);
