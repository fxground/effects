/*
	!Important!
	Don't change the code below!
 */
import "fx-template-js";

let newVal;
const quantityItems = document.querySelectorAll('.quantity');

Array.prototype.slice.call(quantityItems).map(quantityItem => {
	const quantityItemDecrement = quantityItem.querySelector('.quantity__btn--decrement');
	const quantityItemIncrement = quantityItem.querySelector('.quantity__btn--increment');
	const quantityItemInput = quantityItem.querySelector('.quantity__input');
	const quantityValueOld = quantityItem.querySelector('.quantity__value--old');
	const quantityValueNew = quantityItem.querySelector('.quantity__value--new');

	quantityValueOld.textContent = parseInt(quantityItemInput.value);

	quantityItemDecrement.addEventListener('click', function(event) {
		event.preventDefault();

		newVal = parseInt(quantityItemInput.value);
		quantityValueOld.textContent = newVal;
		
		if (newVal > 0) {
			newVal--;
			
			quantityItemInput.value = newVal;
			quantityValueOld.textContent = newVal;

			quantityValueOld.style.top = '-100%';
			quantityValueNew.style.top = '0%';

			quantityItem.classList.add('is--animating');
			quantityItem.classList.add('is--animating-down');

			let animationTimeout = quantityItem.hasAttribute('data-animated') ? 350 : 0;		

			setTimeout(() => {
				quantityValueNew.textContent = newVal;
				quantityItem.classList.remove('is--animating');
				quantityItem.classList.remove('is--animating-down');
				quantityValueOld.style = '';
				quantityValueNew.style = '';
			}, animationTimeout);
		}
	});

	quantityItemIncrement.addEventListener('click', function(event) {
		event.preventDefault();

		newVal = parseInt(quantityItemInput.value);
		quantityValueOld.textContent = newVal;

		newVal++;	

		quantityItemInput.value = newVal;
		quantityValueNew.textContent = newVal;

		quantityValueOld.style.top = '0%';
		quantityValueNew.style.top = '100%';

		quantityItem.classList.add('is--animating');
		quantityItem.classList.add('is--animating-up');

		let animationTimeout = quantityItem.hasAttribute('data-animated') ? 350 : 0;

		setTimeout(() => {
			quantityValueOld.textContent = newVal;
			quantityItem.classList.remove('is--animating');
			quantityItem.classList.remove('is--animating-up');
			quantityValueOld.style = '';
			quantityValueNew.style = '';
		}, animationTimeout);
	});
});

//Enable this code if you need to use jQuery
// ;(function(window, document, $) {
// 	var $win = $(window);
// 	var $doc = $(document);

// 	// Your code goes here...
// 	// jQuery.ready is no longer needed
// })(window, document, window.jQuery);
