/*
	!Important!
	Don't change the code below!
 */
import "fx-template-js";

/**
 * uiProgressButton.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
;( function( window ) {
	
	'use strict';

	var transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		support = { transitions : Modernizr.csstransitions };

	function extend( a, b ) {
		for( var key in b ) { 
			if( b.hasOwnProperty( key ) ) {
				a[key] = b[key];
			}
		}
		return a;
	}

	function SVGEl( el ) {
		this.el = el;
		// the path elements
		this.paths = [].slice.call( this.el.querySelectorAll( 'path' ) );
		// we will save both paths and its lengths in arrays
		this.pathsArr = new Array();
		this.lengthsArr = new Array();
		this._init();
	}

	SVGEl.prototype._init = function() {
		var self = this;
		this.paths.forEach( function( path, i ) {
			self.pathsArr[i] = path;
			path.style.strokeDasharray = self.lengthsArr[i] = path.getTotalLength();
		} );
		// undraw stroke
		this.draw(0);
	}

	// val in [0,1] : 0 - no stroke is visible, 1 - stroke is visible
	SVGEl.prototype.draw = function( val ) {
		for( var i = 0, len = this.pathsArr.length; i < len; ++i ){
			this.pathsArr[ i ].style.strokeDashoffset = this.lengthsArr[ i ] * ( 1 - val );
		}
	}

	function UIProgressButton( el, options ) {
		this.el = el;
		this.options = extend( {}, this.options );
		extend( this.options, options );
		this._init();
	}

	UIProgressButton.prototype.options = {
		// time in ms that the status (success or error will be displayed) - should be at least higher than the transition-duration value defined for the stroke-dashoffset transition of both checkmark and cross strokes 
		statusTime : 1500
	}

	UIProgressButton.prototype._init = function() {
		// the button
		this.button = this.el.querySelector( 'button' );
		// progress el
		this.progressEl = new SVGEl( this.el.querySelector( 'svg.progress-circle' ) );
		// the success/error elems
		this.successEl = new SVGEl( this.el.querySelector( 'svg.checkmark' ) );
		this.errorEl = new SVGEl( this.el.querySelector( 'svg.cross' ) );
		// init events
		this._initEvents();
		// enable button
		this._enable();
	}

	UIProgressButton.prototype._initEvents = function() {
		var self = this;
		this.button.addEventListener( 'click', function() { self._submit(); } );
	}

	UIProgressButton.prototype._submit = function() {
		// by adding the loading class the button will transition to a "circle"
		classie.addClass( this.el, 'loading' );
		
		var self = this,
			onEndBtnTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.propertyName !== 'width' ) return false;
					this.removeEventListener( transEndEventName, onEndBtnTransitionFn );
				}
				
				// disable the button - this should have been the first thing to do when clicking the button.
				// however if we do so Firefox does not seem to fire the transitionend event.
				this.setAttribute( 'disabled', '' );

				if( typeof self.options.callback === 'function' ) {
					self.options.callback( self );
				}
				else {
					// fill it (time will be the one defined in the CSS transition-duration property)
					self.setProgress(1);
					self.stop();
				}
			};

		if( support.transitions ) {
			this.button.addEventListener( transEndEventName, onEndBtnTransitionFn );
		}
		else {
			onEndBtnTransitionFn();
		}
	}

	// runs after the progress reaches 100%
	UIProgressButton.prototype.stop = function( status ) {
		var self = this,
			endLoading = function() {
				// first undraw progress stroke.
				self.progressEl.draw(0);
				
				if( typeof status === 'number' ) {
					var statusClass = status >= 0 ? 'success' : 'error',
						statusEl = status >=0 ? self.successEl : self.errorEl;

					// draw stroke of success (checkmark) or error (cross).
					statusEl.draw( 1 );
					// add respective class to the element
					classie.addClass( self.el, statusClass );
					// after options.statusTime remove status and undraw the respective stroke. Also enable the button.
					setTimeout( function() {
						classie.remove( self.el, statusClass );
						statusEl.draw(0);
						self._enable();
					}, self.options.statusTime );
				}
				else {
					self._enable();
				}
				// finally remove class loading.
				classie.removeClass( self.el, 'loading' );
			};

		// give it a time (ideally the same like the transition time) so that the last progress increment animation is still visible.
		setTimeout( endLoading, 300 );
	}

	UIProgressButton.prototype.setProgress = function( val ) {
		this.progressEl.draw( val );
	}

	// enable button
	UIProgressButton.prototype._enable = function() {
		this.button.removeAttribute( 'disabled' );
	}

	// add to global namespace
	window.UIProgressButton = UIProgressButton;

})( window );

Array.prototype.slice.call( document.querySelectorAll( '.progress-button-circle' ) ).forEach( function( bttn, pos ) {
	new UIProgressButton( bttn, {
		callback : function( instance ) {
			var progress = 0,
				interval = setInterval( function() {
					progress = Math.min( progress + Math.random() * 0.1, 1 );
					instance.setProgress( progress );

					if( progress === 1 ) {
						instance.stop( pos === 1 || pos === 3 ? -1 : 1 );
						clearInterval( interval );
					}
				}, 150 );
		}
	} );
} );

/**
 * progressButton.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */
;( function( window ) {
	
	'use strict';

	// https://gist.github.com/edankwan/4389601
	Modernizr.addTest('csstransformspreserve3d', function () {
		var prop = Modernizr.prefixed('transformStyle');
		var val = 'preserve-3d';
		var computedStyle;
		if(!prop) return false;

		prop = prop.replace(/([A-Z])/g, function(str,m1){ return '-' + m1.toLowerCase(); }).replace(/^ms-/,'-ms-');

		Modernizr.testStyles('#modernizr{' + prop + ':' + val + ';}', function (el, rule) {
			computedStyle = window.getComputedStyle ? getComputedStyle(el, null).getPropertyValue(prop) : '';
		});

		return (computedStyle === val);
	});

	function extend( a, b ) {
		for( var key in b ) { 
			if( b.hasOwnProperty( key ) ) {
				a[key] = b[key];
			}
		}
		return a;
	}

	// support
	var support = { transitions : Modernizr.csstransitions, transforms3d : Modernizr.csstransforms3d && Modernizr.csstransformspreserve3d },
		// transition end event name
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ];

	function ProgressButton( el, options ) {
		this.button = el;
		this.options = extend( {}, this.options );
  		extend( this.options, options );
  		this._init();
	}

	ProgressButton.prototype.options = {
		// time in ms that the status (success or error will be displayed)
		// during this time the button will be disabled
		statusTime : 1500
	};

	ProgressButton.prototype._init = function() {
		this._validate();
		// create structure
		this._create();
		// init events
		this._initEvents();
	};

	ProgressButton.prototype._validate = function() {
		// we will consider the fill/horizontal as default
		if( this.button.getAttribute( 'data-style' ) === null ) {
			this.button.setAttribute( 'data-style', 'fill' );
		}
		if( this.button.getAttribute( 'data-vertical' ) === null && this.button.getAttribute( 'data-horizontal' ) === null ) {
			this.button.setAttribute( 'data-horizontal', '' );
		}
		if( !support.transforms3d && this.button.getAttribute( 'data-perspective' ) !== null ) {
			this.button.removeAttribute( 'data-perspective' );
			this.button.setAttribute( 'data-style', 'fill' );
			this.button.removeAttribute( 'data-vertical' );
			this.button.setAttribute( 'data-horizontal', '' );
		}
	};

	ProgressButton.prototype._create = function() {
		var textEl = document.createElement( 'span' );
		textEl.className = 'content';
		textEl.innerHTML = this.button.innerHTML;
		var progressEl = document.createElement( 'span' );
		progressEl.className = 'progress';

		var progressInnerEl = document.createElement( 'span' );
		progressInnerEl.className = 'progress-inner';
		progressEl.appendChild( progressInnerEl );
		// clear content
		this.button.innerHTML = '';

		if( this.button.getAttribute( 'data-perspective' ) !== null ) {
			var progressWrapEl = document.createElement( 'span' );
			progressWrapEl.className = 'progress-wrap';
			progressWrapEl.appendChild( textEl );
			progressWrapEl.appendChild( progressEl );
			this.button.appendChild( progressWrapEl );
		}
		else {
			this.button.appendChild( textEl );
			this.button.appendChild( progressEl );
		}
		
		// the element that serves as the progress bar
		this.progress = progressInnerEl;

		// property to change on the progress element
		if( this.button.getAttribute( 'data-horizontal' ) !== null ) {
			this.progressProp = 'width';
		}
		else if( this.button.getAttribute( 'data-vertical' ) !== null ) {
			this.progressProp = 'height';
		}
		this._enable();
	};

	ProgressButton.prototype._setProgress = function( val ) {
		this.progress.style[ this.progressProp ] = 100 * val + '%';
	};

	ProgressButton.prototype._initEvents = function() {
		var self = this;
		this.button.addEventListener( 'click', function() {
			// disable the button
			self.button.setAttribute( 'disabled', '' );
			// add class state-loading to the button (applies a specific transform to the button depending which data-style is defined - defined in the stylesheets)
			classie.remove( self.progress, 'notransition' );
			classie.add( this, 'state-loading' );

			setTimeout( function() {
				if( typeof self.options.callback === 'function' ) {
					self.options.callback( self );
				}
				else {
					self._setProgress( 1 );
					var onEndTransFn = function( ev ) {
						if( support.transitions && ev.propertyName !== self.progressProp ) return;
						this.removeEventListener( transEndEventName, onEndTransFn );
						self._stop();
					};
					
					if( support.transitions ) {
						self.progress.addEventListener( transEndEventName, onEndTransFn );
					}
					else {
						onEndTransFn.call();
					}
					
				}
			}, 
			self.button.getAttribute( 'data-style' ) === 'fill' || 
			self.button.getAttribute( 'data-style' ) === 'top-line' ||
			self.button.getAttribute( 'data-style' ) === 'lateral-lines' ? 0 : 200 ); // TODO: change timeout to transitionend event callback
		} );
	};

	ProgressButton.prototype._stop = function( status ) {
		var self = this;

		setTimeout( function() {
			// fade out progress bar
			self.progress.style.opacity = 0;
			var onEndTransFn = function( ev ) {
				if( support.transitions && ev.propertyName !== 'opacity' ) return;
				this.removeEventListener( transEndEventName, onEndTransFn );
				classie.add( self.progress, 'notransition' );
				self.progress.style[ self.progressProp ] = '0%';
				self.progress.style.opacity = 1;
			};

			if( support.transitions ) {
				self.progress.addEventListener( transEndEventName, onEndTransFn );
			}
			else {
				onEndTransFn.call();
			}
			
			
			// add class state-success to the button
			if( typeof status === 'number' ) {
				var statusClass = status >= 0 ? 'state-success' : 'state-error';
				classie.add( self.button, statusClass );
				// after options.statusTime remove status
				setTimeout( function() {
					classie.remove( self.button, statusClass );
					self._enable();
				}, self.options.statusTime );
			}
			else {
				self._enable();
			}

			// remove class state-loading from the button
			classie.remove( self.button, 'state-loading' );
		}, 100 );
	};

	// enable button
	ProgressButton.prototype._enable = function() {
		this.button.removeAttribute( 'disabled' );
	}

	// add to global namespace
	window.ProgressButton = ProgressButton;

})( window );

Array.prototype.slice.call( document.querySelectorAll( 'button.progress-button' ) ).forEach( function( bttn ) {
	new ProgressButton( bttn, {
		callback : function( instance ) {
			var progress = 0,
				interval = setInterval( function() {
					progress = Math.min( progress + Math.random() * 0.1, 1 );
					instance._setProgress( progress );

					if( progress === 1 ) {
						instance._stop(1);
						clearInterval( interval );
					}
				}, 200 );
		}
	} );
} );

//Enable this code if you need to use jQuery
// ;(function(window, document, $) {
// 	var $win = $(window);
// 	var $doc = $(document);

// 	// Your code goes here...
// 	// jQuery.ready is no longer needed
// })(window, document, window.jQuery);
