/*
	!Important!
	Don't change the code below!
 */
import "fx-template-js";

const fields = document.querySelectorAll('.form-control__field');

if(fields) {
	Array.prototype.slice.call(fields).map(field => {
		field.addEventListener('focus', function() {
			field.parentNode.parentNode.classList.add('has-error');
		});

		field.addEventListener('blur', function() {
			field.parentNode.parentNode.classList.remove('has-error');
		});
	});
}

//Enable this code if you need to use jQuery
// ;(function(window, document, $) {
// 	var $win = $(window);
// 	var $doc = $(document);

// 	// Your code goes here...
// 	// jQuery.ready is no longer needed
// })(window, document, window.jQuery);
