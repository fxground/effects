/*
	!Important!
	Don't change the code below!
 */
import "fx-template-js";

// Prepare dropdown markup
const dropdownParents = document.querySelectorAll('li.has-dropdown');

Array.prototype.slice.call(dropdownParents).map(dropdownParent => {
	const backBtn = document.createElement('a');
	const backBtnParent = document.createElement('li');
	
	backBtn.textContent = 'Back';
	backBtn.setAttribute('href', '#')
	backBtn.classList.add('dropdown__back');

	backBtnParent.appendChild(backBtn);

	dropdownParent.querySelector('ul').insertBefore(backBtnParent, dropdownParent.querySelector('ul').firstChild);

	dropdownParent.querySelector('a').addEventListener('click', function(event) {
		event.preventDefault();

		this.parentNode.classList.add('is--opened');
	});

	dropdownParent.querySelector('.dropdown__back').addEventListener('click', function(event) {
		event.preventDefault();

		dropdownParent.classList.remove('is--opened');
	});
});

const dropdownToggle = document.querySelectorAll('.dropdown__toggle');

Array.prototype.slice.call(dropdownToggle).map(toggle => {
	toggle.addEventListener('click', function(event) {
		event.preventDefault();

		toggle.parentNode.querySelector('.dropdown__menu').classList.toggle('is--opened');
	});
});

//Enable this code if you need to use jQuery
// ;(function(window, document, $) {
// 	var $win = $(window);
// 	var $doc = $(document);

// 	// Your code goes here...
// 	// jQuery.ready is no longer needed
// })(window, document, window.jQuery);
