/*
	!Important!
	Don't change the code below!
 */
import "fx-template-js";

/**
 * Search Example #2
 * 
 */
if (document.querySelector('.search--example-2')) {
	const inputEx1 = document.querySelector('.search--example-2 .search__field');
	const btnEx1 = document.querySelector('.search--example-2 .search__btn');
	const formEx1 = document.querySelector('.search--example-2 form');

	btnEx1.addEventListener('click', function(event) {
		event.preventDefault();

		btnEx1.classList.toggle('close');
		inputEx1.classList.toggle('square');
	});

	document.addEventListener('click', function(event) {
		if (!formEx1.contains(event.target)) {
			btnEx1.classList.remove('close');
			inputEx1.classList.remove('square');		
		}	
	});	
}

/**
 * Search example #8
 */
if (document.querySelector('.search--example-8')) {
	const openEx8 = document.querySelector('.search--example-8 .search__open');
	const closeEx8 = document.querySelector('.search--example-8 .search__close');
	const formEx8 = document.querySelector('.search--example-8 .search__form');

	openEx8.addEventListener('click', function(event) {
		event.preventDefault();

		formEx8.classList.add('is--opened');

		setTimeout(() => {
			formEx8.querySelector('.search__field').focus();
		}, 600);
	});

	closeEx8.addEventListener('click', function(event) {
		event.preventDefault();

		formEx8.classList.remove('is--opened');
	});	

	document.addEventListener('keydown', function(event) {
		if (event.keyCode === 27) {
			formEx8.classList.remove('is--opened');
		}
	});
}

/**
 * Search example #9
 */
if (document.querySelector('.search--example-9')) {
	const openEx9 = document.querySelector('.search--example-9 .search__open');
	const closeEx9 = document.querySelector('.search--example-9 .search__close');
	const formEx9 = document.querySelector('.search--example-9 .search__form');

	openEx9.addEventListener('click', function(event) {
		event.preventDefault();

		formEx9.classList.add('is--opened');

		setTimeout(() => {
			formEx9.querySelector('.search__field').focus();
		}, 600);
	});

	closeEx9.addEventListener('click', function(event) {
		event.preventDefault();

		formEx9.classList.remove('is--opened');
	});

	document.addEventListener('keydown', function(event) {
		if (event.keyCode === 27) {
			formEx9.querySelector('.search__field').blur();
			formEx9.classList.remove('is--opened');
		}
	});	
}

/**
 * Search example 10
 */
if (document.querySelector('.search--example-10')) {
	const sample = document.querySelector('.sample--perspective .sample__inner');
	const openEx10 = document.querySelector('.search--example-10 .search__open');
	const closeEx10 = document.querySelector('.search--example-10 .search__close');
	const formEx10 = document.querySelector('.search--example-10 .search__form');

	openEx10.addEventListener('click', function(event) {
		event.preventDefault();

		sample.classList.add('is--transformed');
	});

	closeEx10.addEventListener('click', function(event) {
		event.preventDefault();
		
		sample.classList.remove('is--transformed');
	});

	document.addEventListener('keydown', function(event) {
		if (event.keyCode === 27) {
			sample.classList.remove('is--transformed');
		}
	});
}

//Enable this code if you need to use jQuery
// ;(function(window, document, $) {
// 	var $win = $(window);
// 	var $doc = $(document);

// 	// Your code goes here...
// 	// jQuery.ready is no longer needed
// })(window, document, window.jQuery);
